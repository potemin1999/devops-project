# Jenkins in Jenkins

### What is this?
This repository contains infrastructure-as-a-code for Jenkins development and CD

### How to prepare workspace?
1. Ensure that you host machine has:
    * 15 GB of available disk storage
    * 10 GB of free RAM
    * 10 CPU cores to be used by virtual machines
        
    If your machine does not meet requirements, you are allowed to change Vagrant configuration to the following settings, but in this case system stability is not guaranteed:  
    ```main_node.vm.provider.memory = 1536```  
    ```builder_node.vm.provider.cpus = 2```  
    ```builder_node.vm.provider.memory = 3072```  
    ```staging_node.vm.provider.cpus = 1```  
    ```staging_node.vm.provider.memory = 1024```  
    ```production_node.vm.provider.cpus = 1```  
    ```production_node.vm.provider.memory = 1024```
      
    Therefore the absolute minimum (to hopefully make at least one build) is:
    * 12 GB free disk storage
    * 6.5 GB of RAM
    * 6 CPU cores
2. Ensure that you have installed:
    * ```ansible 2.9.6+```
    * ```python 3.7+```
    * ```vagrant 2.2+```
    * ```virtualbox 5.12+```
3. Run `bash scripts/prepare-workspace.sh`
4. Change the line `executable = <shell-executable>` in /etc/ansible/ansible.cfg to `executable = /bin/bash`
5. Shut down all applications running on ports 8080, 9000, 9100, 9200 of your machine 

### How to deploy infrastructure?
`$ vagrant up`  
Be patient, take your time, popcorn and movies - vagrant doesn't like to hurry

### How to access deployed services?
We have enabled port forwarding for easier access, it can be found in ```Vagrantfile``` on lines 15 and 16
All links here and further are given assuming you have port forwarding enabled.

After the end of deployment process next services are available:  
* Jenkins Automation server as CI server can be found at [0.0.0.0:9000](http://0.0.0.0:9000) and on [10.8.64.8:9000](http://10.8.64.8:9000)  
* Gitea VCS server can be found at [0.0.0.0:8080](http://0.0.0.0:8080) and on [10.8.64.8:8080](http://10.8.64.8:8080)

After first successful build staging and production Jenkins-as-a-Product will be available at:  
* Jenkins staging version - [0.0.0.0:9100](http://0.0.0.0:9100) or [10.8.64.20:8080](http://10.8.64.20:8080)  
* Jenkins production version - [0.0.0.0:9200](http://0.0.0.0:9200) or [10.8.64.30:8080](http://10.8.64.30:8080)  

It does requires setup at the first run, so use corresponding job on Jenkins-as-a-CD to retrieve administrator password

### How to ensure that infrastructure was deployed correctly?
***If any of these conditions are not satisfied, you are welcome to read Troubleshooting section below***   

Login to Gitea VCS server as ```autodevops``` with password ```autodevops```
1. Organization with name jen-in-jen is visible on dashboard page
2. Organization with name jen-in-jen is accessible by click
3. Organization with name jen-in-jen have one repository with name jenkins
3. On page [Jen-In-Jen => Jenkins => Settings => Webhooks](http://0.0.0.0:8080/jen-in-jen/jenkins/settings/hooks) single webhook to localhost:9000 exists
  
Login to Jenkins Automation server as ```admin``` with password ```admin```
1. Three jobs can be seen on main page
2. All three nodes on the left panel are idle, and none of them are offline  
3. [Manage Jenkins](http://0.0.0.0:9000/manage) page does not have red text with notifications about plugins installation errors


### Possible troubleshooting
1. ***Q***: Some steps takes a lot of time, are they stuck?  
   ***A***: No. These steps are usually the ones which requires downloads via network and depends on external services.
      * ```apt-get``` uses default ubuntu repositories to install dependencies
      * Jenkins configuration role downloads Jenkins from their binary releases
      * Gitea configuration role downloads binary release from dl.gitea.io
      * To be ready-to-develop, Gitea starts a migration of Jenkins repository from Github. 
        Since that repository is 140MB+, and github download speed seems to be degraded at 15.05.2020, this step could even fail to be completed automatically
        
2. ***Q***: Jenkins plugins installation is failing  
   ***A1***: Restart provisioning of ```main_node```. 
      For some sad reason, this happens even while manual startup configuration of Jenkins, like they have a highload.  
   ***A2***: Do manual plugin installation via [Jenkins plugin manager](http://0.0.0.0:9000/pluginManager/). 
        
3. ***Q***: Build is running for 15 minutes, is it normal?  
   ***A***: It is not. The first pipeline run does not only build the new version of Jenkins, but also downloads all dependencies needed to complete that task.
      Next builds will be much faster, since they will use cached dependencies.
        
4. ***Q***: I have logged in to Gitea as autodevops user, but there is no jenkins repository for organization jen-in-jen.  
   ***A1***: Try to rerun configuration: ```$ vagrant up main_node --provision```.   
   ***A2***: The repository cloning have failed due to slow github download speed and exceeded Gitea api connection time. You have to do it manually
      * Select "+" on the top action bar of the dashboard page
      * Select new migration on the dropdown menu
      * Fill in the form: 
        * url: ```https://github.com/jenkinsci/jenkins```
        * owner: ```jen-in-jen```
        * repository name: ```jenkins```
        * visibility: ```unchecked``` (public)
        * migration type: ```unchecked``` (not a mirror)
      * Wait till migration is completed
      * Open repository settings => webhooks =>
      * Add webhook for gitea:
        * target url: ```http://localhost:9000/generic-webhook-trigger/invoke?token=jenkins_build_pipeline_token```
        * http method: ```POST```
        * POST content type: select ```application/json```
        * secret: leave empty
        * trigger on: ```push events```
        * branch filter: ```master```
        * active: ```checked``` (webhook is active) 
        
5. ***Q***: Nodes are offline / Pipeline is stuck because the suitable jenkins nodes are offline  
   ***A***: Autoconfiguration and deployment order ensures that jenkins nodes will be ready before master is up.
        The only way, which we found to observe such bug - Virtual Box SCSI controller high-speed data corruption.
        To check whether is it your case, try to login into failed-to-start node via  
        ```$ vagrant ssh <node-name>```,   
        where               
        ```<node-name> = (builder_node|staging_node|production_node|main_node)```  
        If the command fails with errors containing "Input/Output error" - you have to redeploy the node:
        ```$ vagrant destroy <node-name>```  + ```$ vagrant up <node-name```
