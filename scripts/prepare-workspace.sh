#!/bin/bash

echo "Install default Vagrant box"
vagrant box add ubuntu/bionic64 --provider virtualbox

echo "Collect necessary Ansible roles"
ansible-galaxy install -r ansible/requirements.yml

