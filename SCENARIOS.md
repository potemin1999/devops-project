If you haven't read README.md, we suggest you to complete it first.    
To complete scenarios given below you should ensure that infrastructure was deployed correctly and running (follow the corresponding section-checklist in README.md)  
Scenarios will be applied to the project jenkins of organization jen-in-jen on local instance of Gitea VCS, and builds will run on local instance of Jenkins Automation Server
Credentials to these services can be found in README.md

### 1. Trigger build via web interface
* Open main page of Jenkins project in Gitea ([local link](http://0.0.0.0:8080/jen-in-jen/jenkins))  
* Press the button "New File" right above the files list, create any text file in the repository with any content,  
  select "create a new branch for this commit" option and press "Propose file change" button.  
* On the page, which will open after previous step, review your changes and press "New Pull Request".   
  Scroll down the long pull request description (or just delete it) to find and press the button "Create Pull Request"  
* Next page will allow you to merge pull request. Scroll to the end, press "Merge Pull Request", review commit message and confirm merge  
* After you'll see the page with badge "merged", navigate to the Jenkins dashboard to see the push event to master triggering build pipeline

### 2. Trigger build manually 
* Navigate to the dashboard of Jenkins
* Click the job "Jenkins build pipeline"
* Click "Build Now" on the left panel of page
* New build should appear in "Build History"  

### 3. Create dev branch workflow 
___12+ GB of initially free storage is required to complete this scenario and don't get stuck with "no space left on device" problem___
* Navigate to Jenkins project in Gitea and create new branch "dev" via "New File" method, described in Scenario 1
* Go to Jenkins dashboard, click "New Item" on the left panel and create new job via copy from ```jenkins-build-pipeline``` with any name you find suitable for this task
* On the configuration page of the pipeline go to field ```Build Triggers => Generic Webhook Trigger => Token``` and input "jenkins_build_pipeline_token_dev" there
* In build script in the bottom of page, on the line 11 (starting ```git credentialsId: ...```) add ```, branch: 'dev'``` to make git checkout this branch to build onto.
* Scroll till the end of script and remove section ```stage("deploy:production")``` (10 lines), which is used to deploy on production node
* Save pipeline and go back to Jenkins project in Gitea
* On ```Settings => Webhooks``` page create new Gitea webhook with the next settings   
    * target url: ```http://localhost:9000/generic-webhook-trigger/invoke?token=jenkins_build_pipeline_token_dev```
    * http method: ```POST```
    * POST content type: select ```application/json```
    * secret: leave empty
    * trigger on: ```push events```
    * branch filter: ```dev```
    * active: ```checked``` (webhook is active) 
* Now you can save webhook and via merge to branch dev (or via direct push) trigger pipeline which will build dev version of Jenkins and deploy it to staging only

