curl -X POST "http://localhost:8080/api/v1/orgs" \
-H "Authorization: Basic YXV0b2Rldm9wczphdXRvZGV2b3Bz" \
-H "Content-Type: application/json" \
-d "{\"description\":\"Jenkins builds via Jenkins for everyone\", \"username\":\"jen-in-jen\", \"visibility\":\"public\"}"

curl -X POST "http://localhost:8080/api/v1/repos/migrate" \
-H "Authorization: Basic YXV0b2Rldm9wczphdXRvZGV2b3Bz" \
-H "Content-Type: application/json" \
-d "{\"clone_addr\":\"https://github.com/jenkinsci/jenkins\", \"repo_name\":\"jenkins\", \"description\":\"Clone of the official Jenkins repository\",\"uid\":2}"

curl -X POST "http://localhost:8080/api/v1/repos/jen-in-jen/jenkins/hooks" \
-H "Authorization: Basic YXV0b2Rldm9wczphdXRvZGV2b3Bz" \
-H "Content-Type: application/json" \
-d "{\"type\":\"gitea\",\"config\":{\"content_type\":\"json\",\"url\":\"http://localhost:9000/generic-webhook-trigger/invoke?token=jenkins_build_pipeline_token\"},\"events\":[\"push\"],\"branch_filter\":\"master\",\"active\":true}"
