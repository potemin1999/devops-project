def mvnCmd = "mvn -T 1C clean install --batch-mode -Plight-test"

pipeline {
  agent any
  stages {
    stage ('build') {
      agent {
        label 'builder'
      }
      steps {
        git credentialsId: 'autodevops_password_credentials', url: 'http://10.8.64.8:8080/jen-in-jen/jenkins.git'
        sh 'echo "hello"'
        withMaven(mavenOpts: '-Xmx1536m -Xms512m') {
          sh mvnCmd
        }
        dir ('war/target'){
          archiveArtifacts 'jenkins.war'
        }
      }
    }
    stage ('deploy:staging') {
      agent {
        label 'runner && staging'
      }
      steps {
        sh "wget --quiet http://10.8.64.8:9000/job/"+env.JOB_NAME+"/"+env.BUILD_ID+"/artifact/jenkins.war -O jenkins.war"
        sh '(cat .jenkins_pid | while read spo; do sudo kill $spo; done) || echo no product is currently in staging'
        sh 'sudo daemonize -c '+env.WORKSPACE+' -p .jenkins_pid -o out.txt -e err.txt /usr/bin/java -jar jenkins.war '
      }
    }
    stage ('deploy:production') {
      agent {
        label 'runner && production'
      }
      steps {
        sh "wget --quiet http://10.8.64.8:9000/job/"+env.JOB_NAME+"/"+env.BUILD_ID+"/artifact/jenkins.war -O jenkins.war"
        sh '(cat .jenkins_pid | while read spo; do sudo kill $spo; done) || echo no product is currently in production'
        sh 'sudo daemonize -c '+env.WORKSPACE+' -p .jenkins_pid -o out.txt -e err.txt /usr/bin/java -jar jenkins.war '
      }
    }
  }
}
