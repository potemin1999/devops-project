---
- name: "Check gitea version"
  shell: "set -eo pipefail; /usr/local/bin/gitea -v | cut -d' ' -f 3"
  register: gitea_active_version
  changed_when: false
  failed_when: false

- name: "Manually install Gitea 1.11.5 (from dl.gitea.com instead of github.com)"
  get_url:
    url: "https://dl.gitea.io/gitea/1.11.5/gitea-1.11.5-linux-{{ 'amd64' if ansible_architecture == 'x86_64' else ansible_architecture }}"
    dest: /usr/local/bin/gitea
    owner: root
    group: root
    mode: 0755
    force: true
  when: gitea_active_version.stdout != "1.11.5"

- name: "Make gitea running and configured"
  import_role:
    name: thomas_maurice.ansible_role_gitea
  vars:
    gitea_user: "gitea"
    gitea_home: /home/gitea
    gitea_version: "1.11.5"
    gitea_version_check: true
    gitea_app_name: Gitea
    gitea_protocol: true
    gitea_http_listen: 0.0.0.0
    gitea_http_port: 8080
    gitea_offline_mode: true
    gitea_secret_key: gitea_secret_key
    gitea_disable_gravatar: false
    gitea_disable_registration: false
    gitea_require_signin: true
    gitea_db_type: postgres
    gitea_db_host: localhost:5432
    gitea_db_name: gitea_db
    gitea_db_user: gitea_user
    gitea_db_password: password_to_change
    gitea_db_ssl: disable
    gitea_oauth2_enabled: true
    gitea_oauth2_jwt_secret: secret_to_change

- name: "Wait for Gitea to start up before proceeding."
  command: >
    curl -D - --silent --max-time 5 http://localhost:8080/api/v1/version
  args:
    warn: false
  register: result
  until: >
    (result.stdout.find("403 Forbidden") != -1)
    or (result.stdout.find("200 OK") != -1)
    and (result.stdout.find("Please wait while") == -1)
  retries: "30"
  delay: "1"
  changed_when: false
  check_mode: false

- name: "Create autodevops user with admin role"
  become: yes
  become_method: sudo
  become_user: root
  shell: "gitea {{config}} admin create-user {{username}} {{password}} {{email}} --admin true {{pwd_change}}"
  register: gitea_autodevops_user
  changed_when: gitea_autodevops_user.rc == 0
  failed_when: gitea_autodevops_user.rc != 0 and gitea_autodevops_user.rc != 1
  vars:
    config: "--config /etc/gitea/gitea.ini"
    username: "--username autodevops"
    password: "--password autodevops"
    pwd_change: "--must-change-password false"
    email: "--email autodevops@gitea.localhost"

#- name: "Setup organization, users and repositories"
#  script: "templates/basic_setup.sh"
#  environment:
#    AUTODEVOPS_PASSWORD: autodevops

- name: "Migrate Jenkins repository and add webhook"
  script: "templates/clone.sh"
  ignore_errors: yes